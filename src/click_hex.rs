use bevy::{
    input::{mouse::MouseButtonInput, ButtonState},
    prelude::*,
    render::render_asset::RenderAssetUsages,
};
use bevy_inspector_egui::bevy_egui;
use hexx::Hex;
use xcap::image::DynamicImage;

use crate::{
    board::CellState, get_hex_area, image_diff, reference_images::ReferenceImages,
    total_image_diff, vision::BoardScreenshot, BoardState, LAYOUT_IN_BEVY_SPACE,
};

pub struct ClickHexPlugin;
impl Plugin for ClickHexPlugin {
    fn build(&self, app: &mut App) {
        app //.
            .init_resource::<HexInspection>()
            .add_systems(FixedUpdate, inspect_hex_texture_setup)
            .add_systems(Update, (click_hex, inspect_hex, delete_closed));
    }
}

#[derive(Resource, Default)]
pub struct HexInspection {
    hexes: Vec<InspectorWindow>,
}

#[derive(Default)]
struct InspectorWindow {
    hex: Hex,
    keep_open: bool,
    screenshot: Option<TextureHelper>,
    compare: Vec<(Option<CellState>, Option<TextureHelper>, u64)>,
}

impl InspectorWindow {
    pub fn new(hex: Hex) -> InspectorWindow {
        InspectorWindow {
            hex,
            keep_open: true,
            compare: vec![(None, None, 0), (None, None, 0)],
            ..default()
        }
    }
}

pub fn click_hex(
    cameras: Query<(&Camera, &GlobalTransform)>,
    windows: Query<&Window, With<bevy::window::PrimaryWindow>>,
    keyboard: Res<ButtonInput<KeyCode>>,
    mut buttons: EventReader<MouseButtonInput>,
    mut inspection: ResMut<HexInspection>,
) {
    if buttons.is_empty() {
        return;
    }
    let (camera, cam_transform) = cameras.single();
    let window = windows.single();
    if let Some(pos) = window
        .cursor_position()
        .and_then(|p| camera.viewport_to_world_2d(cam_transform, p))
    {
        for click in buttons.read() {
            if click.button == MouseButton::Left
                && click.state == ButtonState::Pressed
                && keyboard.pressed(KeyCode::ShiftLeft)
            {
                let hex = LAYOUT_IN_BEVY_SPACE.world_pos_to_hex(pos);
                if hex.distance_to(Hex::ZERO) > 5 {
                    continue;
                }
                info!("click {:?}", hex);
                inspection.hexes.push(InspectorWindow::new(hex));
            }
        }
    }
}

/// delete closed inspection windows
pub fn delete_closed(mut inspection: ResMut<HexInspection>) {
    inspection.hexes.retain(|x| x.keep_open);
}

struct TextureHelper {
    handle: Handle<Image>,
    texture: bevy_egui::egui::load::SizedTexture,
    image: DynamicImage,
}

impl TextureHelper {
    fn new(
        image: impl Into<DynamicImage>,
        images: &mut ResMut<Assets<Image>>,
        egui_user_textures: &mut ResMut<bevy_egui::EguiUserTextures>,
    ) -> Self {
        use bevy_egui::egui;
        let image = image.into();
        let image2 = Image::from_dynamic(image.clone(), true, RenderAssetUsages::all());
        let handle = images.add(image2);
        Self {
            image,
            handle: handle.clone(),
            texture: egui::load::SizedTexture {
                id: egui_user_textures.add_image(handle),
                size: egui::Vec2::splat(64.),
            },
        }
    }

    fn cleanup(
        self,
        images: &mut ResMut<Assets<Image>>,
        egui_user_textures: &mut ResMut<bevy_egui::EguiUserTextures>,
    ) {
        images.remove(self.handle.clone());
        egui_user_textures.remove_image(&self.handle);
    }
}

fn inspect_hex_texture_setup(
    refer: Res<ReferenceImages>,
    screenshot: Res<BoardScreenshot>,
    mut images: ResMut<Assets<Image>>,
    mut inspection: ResMut<HexInspection>,
    mut egui_user_textures: ResMut<bevy_egui::EguiUserTextures>,
) {
    if !inspection.is_changed() {
        return;
    }
    if let Some(screenshot) = screenshot.0.as_ref() {
        for InspectorWindow {
            hex,
            compare,
            screenshot: helper,
            ..
        } in inspection.hexes.iter_mut()
        {
            info!("updating handle for {:?}", hex);
            let screenshot = get_hex_area(*hex, screenshot);
            if let Some(old) = helper.replace(TextureHelper::new(
                screenshot.to_image(),
                &mut images,
                &mut egui_user_textures,
            )) {
                old.cleanup(&mut images, &mut egui_user_textures);
            };

            for (state, helper, delta) in compare.iter_mut() {
                if let Some(state) = state {
                    let b = refer.get_image_for_state(*hex, *state);
                    *delta = total_image_diff(&*screenshot, b);
                    let new_helper = TextureHelper::new(
                        image_diff(&*screenshot, b),
                        &mut images,
                        &mut egui_user_textures,
                    );
                    if let Some(old) = helper.replace(new_helper) {
                        old.cleanup(&mut images, &mut egui_user_textures)
                    }
                }
            }
        }
    }
}

fn inspect_hex(
    //
    mut inspection: ResMut<HexInspection>,
    mut contexts: bevy_egui::EguiContexts,
    board: Res<BoardState>,
) {
    use bevy_egui::egui;
    // let context = contexts.ctx_mut();
    for inspect in inspection.hexes.iter_mut() {
        egui::Window::new(format!("{:?}", inspect.hex))
            .open(&mut inspect.keep_open)
            .show(contexts.ctx_mut(), |ui| {
                ui.label(format!("Element: {:?}", board.get(inspect.hex)));
                ui.label(format!("Neighbors: {:?}", board.neighbors(inspect.hex)));
                ui.label(format!(
                    "Empty Neighbors: {:?}",
                    board.empty_neighbors(inspect.hex)
                ));
                ui.label(format!(
                    "Adjacent Empty: {}",
                    board.consecutive_empty(inspect.hex)
                ));
                if let Some(helper) = inspect.screenshot.as_ref() {
                    ui.image(helper.texture);
                    if ui.button("save").clicked() {
                        helper
                            .image
                            .save(format!("images/{},{}.png", inspect.hex.x, inspect.hex.y))
                            .unwrap();
                    }
                }
                for (i, (state, helper, delta)) in inspect.compare.iter_mut().enumerate() {
                    // for (mut state, helper) in inspect.compare.iter_mut() {
                    egui::containers::ComboBox::from_label(format!("Compare {}", i))
                        .selected_text(format!("{:?}", state))
                        .show_ui(ui, |sub| {
                            sub.selectable_value(state, None, "Empty");
                            for i in CellState::all() {
                                sub.selectable_value(state, Some(i), format!("{:?}", i));
                            }
                        });
                    ui.label(format!("Delta={:}", delta));
                    if let Some(helper) = helper {
                        ui.image(helper.texture);
                    } else {
                        ui.label("comparison missing");
                    }
                }
            });
    }
}
