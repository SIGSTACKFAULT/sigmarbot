use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui::EguiContexts;
use hexx::Hex;

use crate::{board::BoardState, moves::ScoredMoves, Evaluation, ScoredMove, LAYOUT_IN_BEVY_SPACE};

#[derive(Default, Resource, Clone)]
pub struct DisplayPlugin {
    pub text: bool,
    pub circles: bool,
    pub move_lines: bool,
    pub next_move: bool,
}

#[derive(Component)]
struct DisplayTextParentMarker;

#[derive(Component)]
struct DisplayTextMarker(Hex);

#[derive(Default, Reflect, GizmoConfigGroup)]
struct MainBoardGizmoGroup;

impl Plugin for DisplayPlugin {
    fn build(&self, app: &mut App) {
        app //
            .insert_resource(self.clone())
            .insert_gizmo_group(
                MainBoardGizmoGroup,
                GizmoConfig {
                    line_width: 3.,
                    ..default()
                },
            )
            .add_systems(Startup, setup)
            .add_systems(Update, display_gui)
            .add_systems(Update, update_text)
            .add_systems(Update, draw_current_board)
            .add_systems(Update, draw_next_move);
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let _mesh: bevy::sprite::Mesh2dHandle = meshes.add(Circle::new(10.)).into();
    let _material: Handle<_> = materials.add(Color::PINK);
    commands
        .spawn((SpatialBundle::default(), DisplayTextParentMarker))
        .with_children(|parent| {
            for hex in BoardState::hexes() {
                parent
                    .spawn((
                        SpatialBundle {
                            transform: Transform::from_translation(
                                LAYOUT_IN_BEVY_SPACE.hex_to_world_pos(hex).extend(0.),
                            ),
                            ..Default::default()
                        },
                        Name::new(format!("{:?}", hex)),
                    ))
                    .with_children(|parent| {
                        parent.spawn((
                            Text2dBundle {
                                text: Text::from_section(
                                    "",
                                    TextStyle {
                                        color: Color::BLACK,
                                        ..default()
                                    },
                                ),
                                ..default()
                            },
                            Name::new(format!("Text Display {:?}", hex)),
                            DisplayTextMarker(hex),
                        ));
                    });
            }
        });
}

fn display_gui(mut egui: EguiContexts, mut plugin: ResMut<DisplayPlugin>) {
    use bevy_egui::egui;
    use bevy_inspector_egui::bevy_egui;
    egui::Window::new("Display").show(egui.ctx_mut(), |ui| {
        ui.checkbox(&mut plugin.text, "Text");
        ui.checkbox(&mut plugin.circles, "Circles");
        ui.checkbox(&mut plugin.move_lines, "Move Lines");
        ui.checkbox(&mut plugin.next_move, "Next Board");
    });
}

fn update_text(
    plugin: Res<DisplayPlugin>,
    board: Res<BoardState>,
    mut parent: Query<&mut Visibility, With<DisplayTextParentMarker>>,
    mut texts: Query<(&mut Text, &DisplayTextMarker)>,
) {
    let mut parent = parent.single_mut();
    if plugin.text {
        *parent = Visibility::Visible;
        for (mut text, marker) in texts.iter_mut() {
            use crate::Element::*;
            text.sections[0].value = board
                .get(marker.0)
                .map(|state| match state.inner() {
                    Cardinal(c) => format!("{:?}", c),
                    LifeDeath(ld) => format!("{:?}", ld),
                    Metal(m) => format!("{:?}", m),
                    Salt => "Salt".to_string(),
                    Quicksilver => "Quicksilver".to_string(),
                })
                .unwrap_or_default();
        }
    } else {
        *parent = Visibility::Hidden;
    }
}

fn draw_current_board(
    plugin: Res<DisplayPlugin>,
    board: Res<BoardState>,
    moves: Res<ScoredMoves>,
    mut gizmos: Gizmos<MainBoardGizmoGroup>,
) {
    if plugin.circles {
        draw_board(&mut gizmos, &LAYOUT_IN_BEVY_SPACE, &board);
    }
    if plugin.move_lines {
        draw_moves(&mut gizmos, &LAYOUT_IN_BEVY_SPACE, &moves);
    }
}

/// assumes moves are already sorted high to low
fn draw_board<TGroup: GizmoConfigGroup>(
    gizmos: &mut Gizmos<TGroup>,
    layout: &hexx::HexLayout,
    board: &BoardState,
) {
    for (hex, proxy) in board.iter() {
        let pos = layout.hex_to_world_pos(hex);
        gizmos.circle_2d(pos, layout.hex_size.x * 0.75, proxy.inner().display_color());
    }
}

/// assumes moves are already sorted high to low
fn draw_moves<TGroup: GizmoConfigGroup>(
    gizmos: &mut Gizmos<TGroup>,
    layout: &hexx::HexLayout,
    moves: &ScoredMoves,
) {
    for (i, m) in moves.iter().enumerate() {
        let fraction = 1. - (i as f32 / moves.len() as f32);
        draw_move(
            gizmos,
            layout,
            *m,
            Some(match m.1 {
                Evaluation::Win => Color::GREEN,
                Evaluation::Normal(_) => Color::hsl(0., 0., fraction),
                Evaluation::Lose => Color::RED,
            }),
        );
    }
}

fn draw_move<TGroup: GizmoConfigGroup>(
    gizmos: &mut Gizmos<TGroup>,
    layout: &hexx::HexLayout,
    m: impl Into<ScoredMove>,
    color: Option<Color>,
) {
    let m: ScoredMove = m.into();
    let (a, b) = m.0.as_tuple();
    let color = color.unwrap_or(Color::BLACK);
    if a == b {
        gizmos.circle_2d(layout.hex_to_world_pos(a), layout.hex_size.x, color);
    } else {
        gizmos.line_2d(
            layout.hex_to_world_pos(a),
            layout.hex_to_world_pos(b),
            color,
        );
    }
}

fn draw_next_move(
    plugin: Res<DisplayPlugin>,
    mut gizmos: Gizmos,
    moves: Res<ScoredMoves>,
    board: Res<BoardState>,
    mut cache: Local<Option<(BoardState, ScoredMoves)>>,
) {
    use bevy::prelude::*;
    if !plugin.next_move {
        return;
    }
    if let Some(top) = moves.first() {
        if cache.is_none() || board.is_changed() || moves.is_changed() {
            let board = board.apply_move(top.0);
            let moves = board.find_moves();
            cache.replace((board, moves));
        }
        let temp = cache.as_ref().unwrap();
        let board = &temp.0;
        let moves = &temp.1;
        let small_layout = hexx::HexLayout {
            origin: Vec2::new(-500., 0.),
            hex_size: Vec2::splat(10.),
            orientation: hexx::HexOrientation::Pointy,
            ..default()
        };
        if plugin.circles {
            draw_board(&mut gizmos, &small_layout, board);
        }
        if plugin.move_lines {
            draw_moves(&mut gizmos, &small_layout, moves);
        }
    }
}
