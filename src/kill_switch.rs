use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui;

use crate::{moves::Depth, BoardState};

pub struct KillSwitchPlugin;
impl Plugin for KillSwitchPlugin {
    fn build(&self, app: &mut App) {
        app //.
            .add_systems(FixedUpdate, (detect_lightning, decrement_kill_switch))
            .add_systems(Update, (controls, toggle_hit_detect))
            .init_state::<KillSwitch>()
            .init_resource::<Lightning>();
    }
}

#[derive(States, Default, Debug, Clone, PartialEq, Eq, Hash)]
pub enum KillSwitch {
    #[default]
    Inhibited,
    Cold,
    Hot(u32),
}

impl KillSwitch {
    pub const MAX_HOT: u32 = 30;

    pub fn not_inhibit(state: Res<State<KillSwitch>>) -> bool {
        match **state {
            KillSwitch::Inhibited => false,
            _ => true,
        }
    }
}

#[derive(Resource, Debug, Clone)]
pub struct Lightning(pub Option<xcap::Window>);

impl Lightning {
    fn detect() -> Lightning {
        Lightning(
            xcap::Window::all()
                .unwrap()
                .iter()
                .find(|w| w.app_name().starts_with("Lightning"))
                .map(|w| w.to_owned()),
        )
    }
}

impl FromWorld for Lightning {
    fn from_world(_world: &mut World) -> Self {
        Lightning::detect()
    }
}

pub fn detect_lightning(mut lightning: ResMut<Lightning>, mut next: ResMut<NextState<KillSwitch>>) {
    *lightning = Lightning::detect();
    if lightning.0.is_none() {
        next.set(KillSwitch::Inhibited);
        return;
    }
}

fn controls(
    mut egui: bevy_egui::EguiContexts,
    switch: Res<State<KillSwitch>>,
    mut next: ResMut<NextState<KillSwitch>>,
    mut slider: Local<u32>,
    mut depth: ResMut<Depth>,
    board: Res<BoardState>,
) {
    if *slider == 0 {
        // minimum is 1 so this only happens on init
        *slider = KillSwitch::MAX_HOT;
    }
    use bevy_egui::egui;
    egui::Window::new("Controls").show(egui.ctx_mut(), |ui| {
        ui.label(format!("Kill Switch: {:?}", **switch));
        if ui.button("Single Step").clicked() {
            next.set(KillSwitch::Hot(1));
        }
        if ui.button("Multi Step").clicked() {
            next.set(KillSwitch::Hot(*slider));
        }
        ui.add(egui::widgets::Slider::new(
            &mut (*slider),
            1..=KillSwitch::MAX_HOT,
        ));
        ui.add_space(10.);
        ui.label("Depth");
        ui.add(egui::widgets::Slider::new(&mut depth.0, 0..=Depth::MAX));
        ui.add_space(10.);
        if ui.button("save state").clicked() {
            let saved =
                ron::ser::to_string_pretty(&*board, ron::ser::PrettyConfig::new().depth_limit(1))
                    .unwrap();
            let mut file = std::fs::File::create("board.ron").unwrap();
            use std::io::Write;
            write!(file, "{}", saved).unwrap();
        }
    });
}

fn toggle_hit_detect(switch: Res<State<KillSwitch>>, mut window: Query<&mut Window>) {
    let mut window = window.single_mut();
    match **switch {
        KillSwitch::Hot(_) => {
            window.cursor.hit_test = false;
        }
        KillSwitch::Inhibited | KillSwitch::Cold => {
            window.cursor.hit_test = true;
        }
    }
}

pub fn decrement_kill_switch(
    switch: Res<State<KillSwitch>>,
    mut next: ResMut<NextState<KillSwitch>>,
) {
    match **switch {
        KillSwitch::Hot(0) => next.set(KillSwitch::Cold),
        KillSwitch::Hot(n) => next.set(KillSwitch::Hot(n - 1)),
        _ => (),
    }
}
