use bevy::utils::smallvec::SmallVec;
use hexx::Hex;

use crate::{moves::Move, BoardState, CellState, Element};

impl BoardState {
    /// apply the given `Move`
    /// and return a *new* BoardState
    pub fn apply_move(&self, m: impl Into<Move>) -> BoardState {
        let mut state = self.clone();
        let (h1, h2) = m.into().as_tuple();
        state.remove(h1);
        state.remove(h2);
        // #TODO don't need to check the entire board
        state.update();
        state
    }

    /// unlock marbles as appropriate
    pub fn update(&mut self) {
        let mut to_unlock: SmallVec<[(Hex, Element); 10]> = Default::default();
        for (hex, state) in self.iter() {
            match state {
                CellState::Available(_) => continue,
                CellState::Locked(proxy) => {
                    let adj = self.consecutive_empty(hex);
                    if adj >= 3 {
                        if let Element::Metal(metal) = proxy {
                            if let Some(previous_metal) = metal.prev() {
                                if self.count_element(Element::Metal(previous_metal)) > 0 {
                                    // previous metal still present
                                    continue;
                                }
                            }
                        }
                        to_unlock.push((hex, proxy))
                    }
                }
            }
        }
        for (hex, proxy) in to_unlock {
            self.insert(hex, CellState::Available(proxy));
        }
        // self.retain(|(_h, s)| matches!(s, CellState::Available(_) | CellState::Locked(_)));
    }

    pub fn neighbors(&self, hex: Hex) -> [Option<CellState>; 6] {
        hex.all_neighbors().map(|h| self.get(h))
    }

    pub fn empty_neighbors(&self, hex: Hex) -> [bool; 6] {
        hex.all_neighbors().map(|h| self.get(h).is_none())
    }

    pub fn consecutive_empty(&self, hex: Hex) -> usize {
        Self::consecutive_empty_helper(self.empty_neighbors(hex))
    }

    fn consecutive_empty_helper(empty_neighbors: [bool; 6]) -> usize {
        // lazy algorithm
        let mut longest_chain = 0;
        let mut current_chain = 0;
        for val in empty_neighbors
            .iter()
            // .chain(empty_neighbors.iter())
            // .chain(empty_neighbors.iter())
            .cloned()
        {
            if val {
                current_chain += 1;
                longest_chain = longest_chain.max(current_chain);
            } else {
                current_chain = 0;
            }
        }
        longest_chain
    }
}

#[cfg(test)]
mod test {
    use crate::BoardState;

    #[test]
    fn adjacent_count_test() {
        assert_eq!(
            BoardState::consecutive_empty_helper([false, false, true, true, false, false]),
            2
        );
        assert_eq!(
            BoardState::consecutive_empty_helper([true, true, false, false, false, false]),
            2
        );
        assert_eq!(
            BoardState::consecutive_empty_helper([true, true, true, true, false, false]),
            4
        );
        assert_eq!(
            BoardState::consecutive_empty_helper([false, true, false, false, true, true]),
            2
        );
    }
}
