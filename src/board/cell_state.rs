use crate::Element;
use bevy::prelude::{warn_once, Color};

#[derive(
    Copy, Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, serde::Deserialize, serde::Serialize,
)]
#[allow(dead_code)]
pub enum CellState {
    Locked(Element),
    Available(Element),
}

impl CellState {
    pub fn all() -> impl Iterator<Item = CellState> {
        [].into_iter()
            .chain(Element::iter().map(CellState::Locked))
            .chain(Element::iter().map(CellState::Available))
    }

    pub fn color(self) -> Color {
        warn_once!("CellState::color not implemented");
        Color::PINK
    }

    pub fn inner(&self) -> Element {
        match self {
            CellState::Available(e) => *e,
            CellState::Locked(e) => *e,
        }
    }
}
