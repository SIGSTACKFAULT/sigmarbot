mod apply_move;
mod board_state;
mod cell_state;
mod elements;
mod find_moves;

use bevy::prelude::*;

pub use board_state::BoardState;
pub use cell_state::CellState;
pub use elements::{Cardinal, Element, LifeDeath, Metal};

pub struct BoardPlugin;
impl Plugin for BoardPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<BoardState>();
    }
}
