use crate::{moves::ScoredMoves, BoardState, Element, Move};
use bevy::{prelude::*, utils::smallvec::SmallVec};
use strum::IntoEnumIterator;

use super::elements::{Cardinal, LifeDeath, Metal};

impl BoardState {
    /// find all the moves
    /// all have score zero.
    pub fn find_moves(&self) -> ScoredMoves {
        let mut moves = ScoredMoves::default();
        for element in Cardinal::iter() {
            for (a, b) in unordered_pairs(self.iter_available_element(element)) {
                if a == b {
                    once!({
                        warn!("self-pair {:?} {:?}", a, b);
                        let elements: Vec<_> = self.iter_available_element(element).collect();
                        let pairs = unordered_pairs(elements.iter());
                        warn!("elements: {:?}", elements);
                        warn!("pairs: {:?}", pairs);
                    });
                } else {
                    moves.push(Move::ElementPair(a, b));
                }
            }
        }
        for (a, b) in unordered_pairs(self.iter_available_element(Element::Salt)) {
            moves.push(Move::SaltSalt(a, b));
        }
        for salt in self.iter_available_element(Element::Salt) {
            for (hex, _element) in self.iter_available_cardinals() {
                moves.push(Move::ElementSalt { element: hex, salt })
            }
        }
        for life in self.iter_available_element(LifeDeath::Life) {
            for death in self.iter_available_element(LifeDeath::Death) {
                moves.push(Move::LifeDeath(death, life));
            }
        }
        if let Some((metal, _)) = self.available_metal() {
            for quicksilver in self.iter_available_element(Element::Quicksilver) {
                moves.push(Move::Metal { metal, quicksilver });
            }
        }
        if let Some(_gold) = self.iter_available_element(Metal::Gold).next() {
            moves.push(Move::Gold);
        };
        moves
    }
}

pub fn unordered_pairs<T>(iter: impl Iterator<Item = T>) -> SmallVec<[(T, T); 10]>
where
    T: Clone,
{
    let temp: SmallVec<[T; 10]> = iter.collect();
    temp.iter()
        .enumerate()
        .flat_map(|(i, a)| temp[i + 1..].iter().map(|b| (a.clone(), b.clone())))
        .collect()
}
