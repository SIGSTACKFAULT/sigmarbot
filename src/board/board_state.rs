use crate::{Cardinal, CellState, Element, Metal};
use bevy::prelude::*;
use bevy::utils::smallvec::SmallVec;
use bevy::utils::HashMap;
use hexx::Hex;

#[derive(Resource, Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq, Eq)]
/// pretends to be a map but is actually a vec of tuples.
/// getting a hex is O(n) but the memory savings are worth it
#[serde(into = "HashMap<Hex, CellState>")]
#[serde(from = "HashMap<Hex, CellState>")]
pub struct BoardState(SmallVec<[(Hex, CellState); 55]>);

impl Default for BoardState {
    fn default() -> Self {
        BoardState::new()
    }
}

impl BoardState {
    /// hasn't shipped in hexx yet
    pub const fn hex_as_u64(hex: Hex) -> u64 {
        let high = (hex.x as u32 as u64) << 32;
        let low = hex.y as u32 as u64;
        high | low
    }

    /// wrapper around Vec::binary_search_by_key
    fn binary_search(&self, hex: Hex) -> Result<usize, usize> {
        self.0
            .binary_search_by_key(&Self::hex_as_u64(hex), |(hex, _)| Self::hex_as_u64(*hex))
    }
}

impl BoardState {
    /// new BoardState, with all cells empty
    pub fn new() -> BoardState {
        BoardState(default())
    }

    /// iterator over the sigmar board
    pub fn hexes() -> impl ExactSizeIterator<Item = Hex> {
        hexx::shapes::hexagon(Hex::ZERO, 5)
    }

    pub fn remove(&mut self, hex: Hex) {
        if let Ok(index) = self.binary_search(hex) {
            self.0.remove(index);
        }
    }

    pub fn insert(&mut self, key: Hex, value: CellState) {
        match self.binary_search(key) {
            Ok(index) => {
                self.0[index] = (key, value);
            }
            Err(index) => self.0.insert(index, (key, value)),
        }
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn get(&self, key: Hex) -> Option<CellState> {
        match self.binary_search(key) {
            Ok(index) => Some(self.0[index].1),
            Err(_) => None,
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = (Hex, CellState)> + '_ {
        self.0.iter().copied()
    }

    /// iterate over available proxies
    pub fn iter_available(&self) -> impl Iterator<Item = (Hex, Element)> + '_ {
        use CellState::*;
        self.iter().filter_map(|(hex, state)| match state {
            Locked(_) => None,
            Available(element) => Some((hex, element)),
        })
    }

    pub fn iter_element(&self, element: impl Into<Element>) -> impl Iterator<Item = Hex> + '_ {
        let element = element.into();
        self.iter()
            .filter_map(move |(h, e)| if e.inner() == element { Some(h) } else { None })
    }

    /// count instances of `element`, available or not
    pub fn count_element(&self, element: Element) -> usize {
        self.iter_element(element).count()
    }

    /// iterate over available proxies of the given type
    pub fn iter_available_element(
        &self,
        element: impl Into<Element>,
    ) -> impl Iterator<Item = Hex> + '_ {
        let element = element.into();
        self.iter_available()
            .filter_map(move |(h, e)| if e == element { Some(h) } else { None })
    }

    /// gets the one available metal
    pub fn available_metal(&self) -> Option<(Hex, Metal)> {
        for (hex, element) in self.iter_available() {
            if let Some(metal) = element.into() {
                return Some((hex, metal));
            }
        }
        None
    }

    pub fn iter_available_cardinals(&self) -> impl Iterator<Item = (Hex, Cardinal)> + '_ {
        self.iter_available()
            .filter_map(|(h, e)| <Element as Into<Option<Cardinal>>>::into(e).map(|c| (h, c)))
    }
}

impl From<BoardState> for HashMap<Hex, CellState> {
    fn from(val: BoardState) -> Self {
        val.0.into_iter().collect()
    }
}

impl From<HashMap<Hex, CellState>> for BoardState {
    fn from(value: HashMap<Hex, CellState>) -> Self {
        BoardState(value.into_iter().collect())
    }
}

impl std::fmt::Display for BoardState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut rows: Vec<_> = (0..11).map(|_| " ".repeat(25)).collect();
        for hex in BoardState::hexes() {
            let state = self.get(hex);
            if let Some(_state) = state {
                let [x, y] = hexx::Hex::new(hex.x + 7, hex.y)
                    .to_doubled_coordinates(hexx::DoubledHexMode::DoubledWidth);
                let x = x as usize;
                let y = (y + 5) as usize;
                rows[y].replace_range(
                    x..x + 1,
                    match _state.inner() {
                        Element::Salt => "s",
                        Element::Cardinal(_) => "c",
                        Element::LifeDeath(_) => "l",
                        Element::Metal(_) => "m",
                        Element::Quicksilver => "q",
                    },
                );
            }
        }
        for row in rows {
            writeln!(f, "{}", row)?;
        }
        Ok(())
    }
}
