use bevy::prelude::*;
use strum::IntoEnumIterator;

#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    strum::EnumIter,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum Cardinal {
    Fire,
    Water,
    Earth,
    Air,
}

#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    strum::EnumIter,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum LifeDeath {
    Life,
    Death,
}

#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    strum::EnumIter,
    serde::Deserialize,
    serde::Serialize,
)]
pub enum Metal {
    Lead,
    Tin,
    Iron,
    Copper,
    Silver,
    Gold,
}

impl Metal {
    pub fn next(self) -> Option<Metal> {
        match self {
            Metal::Lead => Some(Metal::Tin),
            Metal::Tin => Some(Metal::Iron),
            Metal::Iron => Some(Metal::Copper),
            Metal::Copper => Some(Metal::Silver),
            Metal::Silver => Some(Metal::Gold),
            Metal::Gold => None,
        }
    }
    pub fn prev(self) -> Option<Metal> {
        match self {
            Metal::Lead => None,
            Metal::Tin => Some(Metal::Lead),
            Metal::Iron => Some(Metal::Tin),
            Metal::Copper => Some(Metal::Iron),
            Metal::Silver => Some(Metal::Copper),
            Metal::Gold => Some(Metal::Silver),
        }
    }
}

#[derive(
    Debug,
    Copy,
    Clone,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    serde::Deserialize,
    serde::Serialize,
    strum::EnumIs,
)]
pub enum Element {
    Salt,
    Cardinal(Cardinal),
    LifeDeath(LifeDeath),
    Quicksilver,
    Metal(Metal),
}

impl Element {
    pub fn iter() -> impl Iterator<Item = Element> {
        [Element::Salt, Element::Quicksilver]
            .into_iter()
            .chain(Cardinal::iter().map(Element::Cardinal))
            .chain(LifeDeath::iter().map(Element::LifeDeath))
            .chain(Metal::iter().map(Element::Metal))
    }
    pub fn display_color(self) -> Color {
        match self {
            Element::Salt => Color::WHITE,
            Element::Quicksilver => Color::ORANGE,
            Element::Cardinal(Cardinal::Air) => Color::rgb_u8(0x88, 0x88, 0xff),
            Element::Cardinal(Cardinal::Earth) => Color::rgb_u8(0x00, 0xaa, 0x00),
            Element::Cardinal(Cardinal::Water) => Color::rgb_u8(0x00, 0x00, 0xaa),
            Element::Cardinal(Cardinal::Fire) => Color::rgb_u8(0xff, 0x00, 0x00),
            Element::LifeDeath(_) => Color::BLACK,
            Element::Metal(_) => Color::PINK,
        }
    }
}

impl std::fmt::Display for Element {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match *self {
            Element::Salt => "Salt",
            Element::Cardinal(Cardinal::Air) => "Air",
            Element::Cardinal(Cardinal::Earth) => "Earth",
            Element::Cardinal(Cardinal::Water) => "Water",
            Element::Cardinal(Cardinal::Fire) => "Fire",
            Element::LifeDeath(LifeDeath::Death) => "Death",
            Element::LifeDeath(LifeDeath::Life) => "Life",
            Element::Quicksilver => "Quicksilver",
            Element::Metal(Metal::Lead) => "Lead",
            Element::Metal(Metal::Tin) => "Tin",
            Element::Metal(Metal::Iron) => "Iron",
            Element::Metal(Metal::Copper) => "Copper",
            Element::Metal(Metal::Silver) => "Silver",
            Element::Metal(Metal::Gold) => "Gold",
        })
    }
}

impl From<Metal> for Element {
    fn from(value: Metal) -> Self {
        Element::Metal(value)
    }
}

impl From<Cardinal> for Element {
    fn from(value: Cardinal) -> Self {
        Element::Cardinal(value)
    }
}

impl From<LifeDeath> for Element {
    fn from(value: LifeDeath) -> Self {
        Element::LifeDeath(value)
    }
}

impl From<Element> for Option<Metal> {
    fn from(val: Element) -> Self {
        match val {
            Element::Metal(m) => Some(m),
            _ => None,
        }
    }
}

impl From<Element> for Option<Cardinal> {
    fn from(val: Element) -> Self {
        match val {
            Element::Cardinal(c) => Some(c),
            _ => None,
        }
    }
}
