mod plural;
mod singular;

use crate::{
    board::BoardState, evaluation::recursive_evaluate, kill_switch::KillSwitch,
    LAYOUT_IN_MAGNUM_SPACE,
};
use bevy::prelude::*;
use hexx::Hex;
use std::{thread::sleep, time::Duration};

pub use plural::ScoredMoves;
pub use singular::{Move, ScoredMove};

pub struct MovesPlugin;
impl Plugin for MovesPlugin {
    fn build(&self, app: &mut App) {
        app //
            .init_resource::<ScoredMoves>()
            .insert_resource(Depth(3))
            .add_systems(FixedUpdate, find_moves.after(super::determine_elements))
            .add_systems(FixedUpdate, score_moves.after(find_moves))
            .add_systems(
                FixedUpdate,
                make_move
                    .run_if(|state: Res<State<KillSwitch>>| matches!(**state, KillSwitch::Hot(_)))
                    .after(crate::kill_switch::decrement_kill_switch)
                    .after(score_moves),
            );
    }
}

#[derive(Clone, Default, Resource, Deref)]
pub struct Depth(pub usize);
impl Depth {
    pub const MAX: usize = 4;
}

fn find_moves(board: Res<BoardState>, mut moves: ResMut<ScoredMoves>) {
    *moves = board.find_moves();
}

pub fn score_moves(board: Res<BoardState>, mut moves: ResMut<ScoredMoves>, depth: Res<Depth>) {
    use rayon::prelude::*;
    moves.par_iter_mut().for_each(|m| {
        let board = board.apply_move(*m);
        let evaluation = recursive_evaluate(depth.0, &board);
        m.1 = evaluation;
    });
    moves.sort();
}

pub fn make_move(mut next: ResMut<NextState<KillSwitch>>, moves: Res<ScoredMoves>) {
    click_hex(Hex::new(-7, 0));
    let Some(chosen_one) = moves.first() else {
        info!("no moves");
        // click_hex(Hex::new(-7, -5)); // new game
        next.set(KillSwitch::Cold);
        return;
    };
    info!("chosen one: {:2?}", chosen_one);
    chosen_one.0.execute();
    click_hex(Hex::new(-7, 0));
}

pub fn click_hex(hex: Hex) {
    let Vec2 { x, y } = LAYOUT_IN_MAGNUM_SPACE.hex_to_world_pos(hex);
    let y = y + 10.;
    let delay = Duration::from_millis(20);
    rdev::simulate(&rdev::EventType::MouseMove {
        x: x as f64,
        y: y as f64,
    })
    .unwrap();
    sleep(delay);
    rdev::simulate(&rdev::EventType::ButtonPress(rdev::Button::Left)).unwrap();
    sleep(delay);
    rdev::simulate(&rdev::EventType::ButtonRelease(rdev::Button::Left)).unwrap();
    sleep(delay);
}
