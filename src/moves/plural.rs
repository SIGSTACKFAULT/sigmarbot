use super::ScoredMove;
use bevy::prelude::{Deref, DerefMut, Resource};
use bevy::utils::smallvec::SmallVec;

const MOVES_SIZE: usize = 20;

#[derive(Clone, Debug, Resource, Default, Deref, DerefMut)]
pub struct ScoredMoves(pub SmallVec<[ScoredMove; MOVES_SIZE]>);

impl ScoredMoves {
    pub fn new(moves: impl IntoIterator<Item = ScoredMove>) -> Self {
        Self(moves.into_iter().collect())
    }

    pub fn push(&mut self, m: impl Into<ScoredMove>) {
        self.0.push(m.into());
    }

    pub fn sort(&mut self) {
        self.0.sort_by_key(|m| {
            let score: i32 = m.1.into();
            score.saturating_neg()
        });
        // if self.len() >= 2 && self[0].1 < self[1].1 {
        //     warn_once!("sort is wrong way!");
        // }
    }

    pub fn best(&mut self) -> Option<ScoredMove> {
        // self.sort();
        // self.first().cloned()
        self.iter()
            .reduce(|a, b| if a.1 > b.1 { a } else { b })
            .cloned()
    }
}
