use hexx::Hex;

use crate::evaluation::Evaluation;

use super::click_hex;

#[derive(Copy, Clone, Debug)]
pub enum Move {
    ElementPair(Hex, Hex),
    ElementSalt { element: Hex, salt: Hex },
    SaltSalt(Hex, Hex),
    LifeDeath(Hex, Hex),
    Metal { metal: Hex, quicksilver: Hex },
    Gold,
}

impl Move {
    /// as a 2-tuple of Hexes
    /// Gold returns (Hex::ZERO, Hex::ZERO)
    pub fn as_tuple(&self) -> (Hex, Hex) {
        match *self {
            Move::ElementPair(a, b)
            | Move::LifeDeath(a, b)
            | Move::SaltSalt(a, b)
            | Move::ElementSalt {
                element: a,
                salt: b,
            }
            | Move::Metal {
                metal: a,
                quicksilver: b,
            } => (a, b),
            Move::Gold => (Hex::ZERO, Hex::ZERO),
        }
    }

    /// list of hexes adjacent to proxies removed by this
    pub fn adjacencies(&self) -> Vec<Hex> {
        let (a, b) = self.as_tuple();
        let mut result = a
            .all_neighbors()
            .into_iter()
            .chain(b.all_neighbors())
            .collect::<Vec<_>>();
        result.sort_by_key(|h| h.x + (h.y << 31));
        result.dedup();
        result
    }

    pub fn execute(self) {
        match self {
            Move::ElementPair(a, b)
            | Move::LifeDeath(a, b)
            | Move::SaltSalt(a, b)
            | Move::ElementSalt {
                element: a,
                salt: b,
            }
            | Move::Metal {
                metal: a,
                quicksilver: b,
            } => {
                click_hex(a);
                click_hex(b);
            }
            Move::Gold => {
                click_hex(Hex::ZERO);
            }
        }
    }
}

#[derive(Copy, Clone, Debug, bevy::prelude::Deref, bevy::prelude::DerefMut)]
pub struct ScoredMove(#[deref] pub Move, pub Evaluation);

impl ScoredMove {
    /// geuss how good the move is, without looking at the board
    pub fn heuristic(&self) -> isize {
        self.score_move_type()
    }

    /// score just based on the move type
    pub fn score_move_type(&self) -> isize {
        match self.0 {
            Move::Gold => 4,
            Move::Metal {
                metal: _,
                quicksilver: _,
            } => 3,
            Move::ElementPair(_, _) | Move::SaltSalt(_, _) | Move::LifeDeath(_, _) => 2,
            Move::ElementSalt {
                element: _,
                salt: _,
            } => 1,
        }
    }
}

impl From<ScoredMove> for Move {
    fn from(value: ScoredMove) -> Self {
        value.0
    }
}

impl From<Move> for ScoredMove {
    fn from(value: Move) -> Self {
        ScoredMove(value, Evaluation::ZERO)
    }
}
