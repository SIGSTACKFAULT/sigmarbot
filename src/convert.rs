use bevy::render::color::Color;
use oklab::Oklab;
use xcap::image::{Rgb, Rgba};

#[allow(dead_code)]
pub fn rgba_to_color(rgba: Rgba<u8>) -> Color {
    let Rgba([r, g, b, _a]) = rgba;
    Color::rgb_u8(r, g, b)
}

#[allow(dead_code)]
pub fn rgb_to_color(rgb: Rgb<u8>) -> Color {
    let Rgb([r, g, b]) = rgb;
    Color::rgb_u8(r, g, b)
}

#[allow(dead_code)]
pub fn color_to_rgba(color: Color) -> Rgba<u8> {
    let [r, g, b, _a] = color.as_rgba_u8();
    Rgba([r, g, b, 0xff])
}

#[allow(dead_code)]
pub fn color_to_oklab(color: Color) -> Oklab {
    let [r, g, b, _a] = color.as_rgba_u8();
    oklab::srgb_to_oklab(oklab::RGB { r, g, b })
}
