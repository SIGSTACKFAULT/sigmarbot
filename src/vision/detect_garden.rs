use bevy::prelude::*;
use bevy::utils::tracing;
use hexx::Hex;
use xcap::image::{self, GenericImageView};

use super::BoardScreenshot;
use crate::kill_switch::KillSwitch;
use crate::{get_hex_area, total_image_diff};

const GARDEN_DETECT_HEX: Hex = Hex::new(-8, 5);

#[derive(Resource)]
/// a bit of the Garden ui off the board
/// which we compare to check that Garden is actually open
pub struct GardenDetectionReference(image::RgbaImage);
impl Default for GardenDetectionReference {
    fn default() -> Self {
        GardenDetectionReference(
            image::io::Reader::open("images/kill_switch.png")
                .unwrap()
                .decode()
                .unwrap()
                .into_rgba8(),
        )
    }
}

/// check the hex at [`GARDEN_DETECT_HEX`],
/// which is off the board but we don't care,
/// and compare it to GardenDetectionReference.
/// then set KillSwitch
#[tracing::instrument(skip_all)]
pub fn detect_garden(
    reference: Res<GardenDetectionReference>,
    screenshot: Res<BoardScreenshot>,
    current: Res<State<KillSwitch>>,
    mut kill_switch: ResMut<NextState<KillSwitch>>,
) {
    // #TODO avoid cloning
    let Some(screenshot) = screenshot.0.as_ref() else {
        info!("inhibiting due to screenshot");
        kill_switch.set(KillSwitch::Inhibited);
        return;
    };
    let area = get_hex_area(GARDEN_DETECT_HEX, &screenshot);
    let diff = total_image_diff(&*area, &*reference.0.view(0, 0, 32, 32));
    if diff > 1000 {
        info!("inhibiting due to garden");
        kill_switch.set(KillSwitch::Inhibited);
        return;
    }
    // if we get here we're happy
    if **current == KillSwitch::Inhibited {
        info!("going cold");
        kill_switch.set(KillSwitch::Cold);
    }
}
