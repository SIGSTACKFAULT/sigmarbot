//! Stuff what compares images
//! also takes the screenshot

// mod burn_model;
mod detect_garden;
pub mod image_diff;
mod take_screenshot;

use bevy::prelude::*;
use bevy::utils::tracing;
pub use detect_garden::detect_garden;
use hexx::Hex;
use xcap::image;

use crate::{kill_switch, reference_images::ReferenceImages, total_image_diff, CellState};

pub struct Vision;
impl Plugin for Vision {
    fn build(&self, app: &mut App) {
        app //
            .init_resource::<BoardScreenshot>()
            .init_resource::<detect_garden::GardenDetectionReference>()
            .init_resource::<ScreenshotLayout>()
            .init_resource::<take_screenshot::TakeScreenshotState>()
            .add_systems(FixedUpdate,
        take_screenshot::start_screenshot.after(
            crate::moves::make_move
        ).after(crate::moves::score_moves))
            .add_systems(FixedUpdate, (//
                detect_garden, apply_state_transition::<kill_switch::KillSwitch>).chain().after(kill_switch::decrement_kill_switch))
            .add_systems(Update, take_screenshot::poll_screenshot)
        //.
        ;
    }
}

#[derive(Resource, Deref, Default, Debug)]
pub struct BoardScreenshot(pub Option<image::RgbaImage>);

#[derive(Resource, Deref, Default, Debug)]
pub struct ScreenshotLayout(pub hexx::HexLayout);

#[tracing::instrument(skip(cell, reference))]
pub fn identify(
    hex: Hex,
    cell: image::SubImage<&image::RgbaImage>,
    reference: &ReferenceImages,
) -> Option<CellState> {
    let average_color = average_color(cell);
    let expected_average = reference.empty_average_colors.get(&hex).unwrap().to_owned();
    if expected_average == average_color {
        return None;
    } else {
        let mut best_state = None;
        let mut best_delta = u64::MAX;
        for (state, image) in reference.get_images_for_hex(hex).iter() {
            let delta = total_image_diff(*image, &*cell);
            if delta == 0 {
                // bingo
                return Some(*state);
            }
            if delta < best_delta {
                best_state = Some(*state);
                best_delta = delta;
            }
        }
        best_state
    }
}

pub fn average_color(cell: image::SubImage<&image::RgbaImage>) -> [u8; 4] {
    let w = cell.width();
    let h = cell.width();
    let size = w * h;
    let mut sum_r = 0;
    let mut sum_g = 0;
    let mut sum_b = 0;
    use image::GenericImageView;
    for x in 0..cell.width() {
        for y in 0..cell.height() {
            sum_r += cell.get_pixel(x, y).0[0] as u32;
            sum_g += cell.get_pixel(x, y).0[1] as u32;
            sum_b += cell.get_pixel(x, y).0[2] as u32;
        }
    }
    [
        (sum_r / size) as u8,
        (sum_g / size) as u8,
        (sum_b / size) as u8,
        255,
    ]
}
