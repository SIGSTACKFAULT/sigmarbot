use bevy::{prelude::*, utils::tracing};
use xcap::image::{self, Rgba};

#[inline]
pub fn colour_diff(a: image::Rgba<u8>, b: image::Rgba<u8>) -> u64 {
    let mut diff: u64 = 0;
    diff += u8::abs_diff(a[0], b[0]) as u64;
    diff += u8::abs_diff(a[1], b[1]) as u64;
    diff += u8::abs_diff(a[2], b[2]) as u64;
    diff += {
        let a = Color::rgb_u8(a[0], a[1], a[2]);
        let b = Color::rgb_u8(b[0], b[1], b[2]);
        (f32::abs(a.h() - b.h())) as u64
    };
    diff
}

#[tracing::instrument(skip_all)]
pub fn image_diff(
    a: &impl image::GenericImageView<Pixel = Rgba<u8>>,
    b: &impl image::GenericImageView<Pixel = Rgba<u8>>,
) -> image::GrayImage {
    assert_eq!(a.width(), b.width());
    assert_eq!(a.width(), b.width());
    let mut result = image::GrayImage::new(a.width(), a.height());
    let width = a.width();
    let height = b.height();
    for x in 0..width {
        for y in 0..height {
            let pixel_diff = colour_diff(a.get_pixel(x, y), b.get_pixel(x, y));
            result.put_pixel(
                x,
                y,
                image::Luma([pixel_diff.clamp(0, u8::MAX as u64) as u8]),
            );
        }
    }
    result
}

#[tracing::instrument(skip_all)]
pub fn total_image_diff(
    a: &impl image::GenericImageView<Pixel = Rgba<u8>>,
    b: &impl image::GenericImageView<Pixel = Rgba<u8>>,
) -> u64 {
    assert_eq!(a.width(), b.width());
    assert_eq!(a.height(), b.height());
    let w = a.width();
    let h = a.height();
    let mut total: u64 = 0;
    for x in 0..w {
        for y in 0..h {
            total += colour_diff(a.get_pixel(x, y), b.get_pixel(x, y));
        }
    }
    total
}
