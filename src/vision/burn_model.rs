use crate::CellState;
use burn::{
    data::{dataloader::Dataset, dataset::vision::ImageFolderDataset},
    nn::{
        conv::{Conv2d, Conv2dConfig},
        pool::{AdaptiveAvgPool2d, AdaptiveAvgPool2dConfig},
        Dropout, DropoutConfig, Linear, LinearConfig, Relu,
    },
    prelude::*,
};
use xcap::image;

const NUM_CLASSES: usize = (14 * 2) + 1; // (14 elements * available/locked) + empty

pub fn identify(screenshot: image::RgbaImage) -> Option<CellState> {
    assert_eq!(screenshot.width(), 32);
    assert_eq!(screenshot.height(), 32);
    let dataset = dataset();
    println!("{:?}", dataset.len());
    for item in dataset.iter() {
        println!("{:?}", item.annotation);
    }
    todo!();
}

#[derive(Module, Debug)]
pub struct Model<B: Backend> {
    conv1: Conv2d<B>,
    conv2: Conv2d<B>,
    pool: AdaptiveAvgPool2d,
    dropout: Dropout,
    linear1: Linear<B>,
    linear2: Linear<B>,
    activation: Relu,
}

#[derive(Config, Debug)]
pub struct ModelConfig {
    hidden_size: usize,
    #[config(default = "0.5")]
    dropout: f64,
}

impl ModelConfig {
    /// Returns the initialized model.
    pub fn init<B: Backend>(&self, device: &B::Device) -> Model<B> {
        Model {
            conv1: Conv2dConfig::new([1, 8], [3, 3]).init(device),
            conv2: Conv2dConfig::new([8, 16], [3, 3]).init(device),
            pool: AdaptiveAvgPool2dConfig::new([8, 8]).init(),
            dropout: DropoutConfig::new(self.dropout).init(),
            linear1: LinearConfig::new(16 * 8 * 8, self.hidden_size).init(device),
            linear2: LinearConfig::new(self.hidden_size, NUM_CLASSES).init(device),
            activation: Relu::new(),
        }
    }
}

impl<B: Backend> Model<B> {
    /// # Shapes
    ///   - Images [batch_size, height, width]
    ///   - Output [batch_size, num_classes]
    pub fn forward(&self, images: Tensor<B, 3>) -> Tensor<B, 2> {
        let [batch_size, height, width] = images.dims();

        // Create a channel at the second dimension.
        let x = images.reshape([batch_size, 1, height, width]);

        let x = self.conv1.forward(x); // [batch_size, 8, _, _]
        let x = self.dropout.forward(x);
        let x = self.conv2.forward(x); // [batch_size, 16, _, _]
        let x = self.dropout.forward(x);
        let x = self.activation.forward(x);

        let x = self.pool.forward(x); // [batch_size, 16, 8, 8]
        let x = x.reshape([batch_size, 16 * 8 * 8]);
        let x = self.linear1.forward(x);
        let x = self.dropout.forward(x);
        let x = self.activation.forward(x);

        self.linear2.forward(x) // [batch_size, num_classes]
    }
}

fn dataset() -> ImageFolderDataset {
    ImageFolderDataset::new_classification("images").unwrap()
}

#[cfg(test)]
#[test]
fn correct_number_of_labels() {
    use burn::data::dataset::vision::Annotation;
    let dataset = dataset();
    let mut highest = 0;
    for item in dataset.iter() {
        match item.annotation {
            Annotation::Label(n) => highest = highest.max(n),
            _ => panic!(),
        }
    }
    assert_eq!(highest, NUM_CLASSES);
}

#[derive(Clone)]
pub struct Batcher<B: Backend> {
    device: B::Device,
}

impl<B: Backend> Batcher<B> {
    pub fn new(device: B::Device) -> Self {
        Self { device }
    }
}

mod training {
    use super::{dataset, ModelConfig};
    use burn::{
        data::dataloader::DataLoaderBuilder,
        optim::AdamConfig,
        prelude::*,
        record::CompactRecorder,
        tensor::backend::AutodiffBackend,
        train::{
            metric::{AccuracyMetric, LossMetric},
            LearnerBuilder,
        },
    };

    #[derive(Config)]
    pub struct TrainingConfig {
        pub model: ModelConfig,
        pub optimizer: AdamConfig,
        #[config(default = 10)]
        pub num_epochs: usize,
        #[config(default = 64)]
        pub batch_size: usize,
        #[config(default = 4)]
        pub num_workers: usize,
        #[config(default = 42)]
        pub seed: u64,
        #[config(default = 1.0e-4)]
        pub learning_rate: f64,
    }
    fn create_artifact_dir(artifact_dir: &str) {
        // Remove existing artifacts before to get an accurate learner summary
        std::fs::remove_dir_all(artifact_dir).ok();
        std::fs::create_dir_all(artifact_dir).ok();
    }

    pub fn train<B: AutodiffBackend>(
        artifact_dir: &str,
        config: TrainingConfig,
        device: B::Device,
    ) {
        create_artifact_dir(artifact_dir);
        config
            .save(format!("{artifact_dir}/config.json"))
            .expect("Config should be saved successfully");

        B::seed(config.seed);

        let batcher_train = ClassificationBatcher;
        let batcher_valid = MnistBatcher::<B::InnerBackend>::new(device.clone());

        let dataloader_train = DataLoaderBuilder::new(batcher_train)
            .batch_size(config.batch_size)
            .shuffle(config.seed)
            .num_workers(config.num_workers)
            .build(dataset());

        let dataloader_test = dataloader_train.clone();

        let learner = LearnerBuilder::new(artifact_dir)
            .metric_train_numeric(AccuracyMetric::new())
            .metric_valid_numeric(AccuracyMetric::new())
            .metric_train_numeric(LossMetric::new())
            .metric_valid_numeric(LossMetric::new())
            .with_file_checkpointer(CompactRecorder::new())
            .devices(vec![device.clone()])
            .num_epochs(config.num_epochs)
            .summary()
            .build(
                config.model.init::<B>(&device),
                config.optimizer.init(),
                config.learning_rate,
            );

        let model_trained = learner.fit(dataloader_train, dataloader_test);

        model_trained
            .save_file(format!("{artifact_dir}/model"), &CompactRecorder::new())
            .expect("Trained model should be saved successfully");
    }
}
