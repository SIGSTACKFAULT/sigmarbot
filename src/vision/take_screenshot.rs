use super::BoardScreenshot;
use bevy::prelude::*;
use bevy::utils::tracing;

#[derive(Resource, Default, Debug)]
pub enum TakeScreenshotState {
    #[default]
    Idle,
    Working(bevy::tasks::Task<Option<xcap::image::RgbaImage>>),
}

#[tracing::instrument(skip(lightning))]
pub fn start_screenshot(
    lightning: Res<crate::kill_switch::Lightning>,
    mut state: ResMut<TakeScreenshotState>,
) {
    let pool = bevy::tasks::AsyncComputeTaskPool::get();
    match lightning.0.as_ref() {
        None => {}
        Some(window) => {
            let window = window.clone();
            let task = pool.spawn(async move {
                let _span = bevy::utils::tracing::info_span!("take_screenshot");
                let _enter = _span.enter();
                match window.capture_image() {
                    Ok(capture) => Some(capture),
                    Err(e) => {
                        warn!("Failed to capture image: {}", e);
                        None
                    }
                }
            });
            *state = TakeScreenshotState::Working(task);
        }
    }
}

#[tracing::instrument(skip(latest_screenshot))]
pub fn poll_screenshot(
    mut state: ResMut<TakeScreenshotState>,
    mut latest_screenshot: ResMut<BoardScreenshot>,
) {
    match state.as_mut() {
        TakeScreenshotState::Idle => {}
        TakeScreenshotState::Working(task) => {
            if let Some(result) = bevy::tasks::block_on(bevy::tasks::poll_once(task)) {
                latest_screenshot.0 = result;
                *state = TakeScreenshotState::Idle;
            }
        }
    };
}
