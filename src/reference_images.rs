use crate::{CellState, Element};
use bevy::prelude::*;
use bevy::utils::HashMap;
use hexx::Hex;
use xcap::image::{self, RgbaImage};

#[derive(Resource)]
pub struct ReferenceImages {
    pub elements: HashMap<CellState, Vec<(Hex, image::RgbaImage)>>,
    pub empty_average_colors: HashMap<hexx::Hex, [u8; 4]>,
}

impl Default for ReferenceImages {
    fn default() -> Self {
        fn dir_to_images(path: &str) -> Vec<(Hex, image::RgbaImage)> {
            let re = regex::Regex::new(r"(-?\d+),(-?\d+)").unwrap();
            std::fs::read_dir(path)
                .unwrap()
                .filter_map(Result::ok)
                .filter_map(|entry| {
                    let open = image::open(entry.path()).unwrap().into_rgba8();
                    let file_name = entry.file_name();
                    let file_name = file_name.to_string_lossy();
                    if file_name.ends_with(".png") {
                        if let Some(captured) = re.captures(&entry.file_name().to_string_lossy()) {
                            Some((
                                Hex::new(
                                    captured[1].parse().unwrap(),
                                    captured[2].parse().unwrap(),
                                ),
                                open,
                            ))
                        } else {
                            warn!(
                                "falling back to Hex::ZERO for {}",
                                entry.path().to_string_lossy()
                            );
                            Some((Hex::ZERO, open))
                        }
                    } else {
                        None
                    }
                })
                .collect()
        }
        let mut elements = HashMap::new();
        for (e, imgs) in
            Element::iter().map(|e| (e, dir_to_images(&format!("images/Available/{}/", e))))
        {
            elements.insert(CellState::Available(e), imgs);
        }
        for (e, imgs) in
            Element::iter().map(|e| (e, dir_to_images(&format!("images/Locked/{}/", e))))
        {
            elements.insert(CellState::Locked(e), imgs);
        }
        ReferenceImages {
            empty_average_colors: ron::from_str(include_str!("../images/colors.ron")).unwrap(),
            elements,
        }
    }
}

impl ReferenceImages {
    pub fn get_image_for_state<'a>(&'a self, hex: Hex, state: CellState) -> &'a RgbaImage {
        let this_state = self.elements.get(&state).unwrap();
        this_state
            .iter()
            .map(|(h, img)| (h.distance_to(hex), img))
            .reduce(|a, b| if a.0 < b.0 { a } else { b })
            .unwrap()
            .1
    }
    pub fn get_images_for_hex<'a>(&'a self, hex: Hex) -> HashMap<CellState, &'a RgbaImage> {
        let mut result = HashMap::new();
        for e in Element::iter() {
            result.insert(
                CellState::Available(e),
                self.get_image_for_state(hex, CellState::Available(e)),
            );
            result.insert(
                CellState::Locked(e),
                self.get_image_for_state(hex, CellState::Locked(e)),
            );
        }
        result
    }
}
