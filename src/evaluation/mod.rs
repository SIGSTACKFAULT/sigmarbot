mod display;
#[allow(clippy::module_inception)]
mod evaluation;
mod node;
mod strategy;

use std::{collections::BinaryHeap, sync::Arc};

use crate::BoardState;
pub use display::DisplayEvals;
pub use evaluation::Evaluation;
pub use node::EvalNode;
pub use strategy::Strategy;

pub struct EvalState {
    pub root: Arc<EvalNode>,
    pub queue: BinaryHeap<Arc<EvalNode>>,
}

#[allow(dead_code)]
impl EvalState {
    pub fn new(depth: usize, board: BoardState) -> EvalState {
        let root = Arc::new(EvalNode::new(depth, board, None));
        EvalState {
            root: root.clone(),
            queue: [root].into(),
        }
    }

    pub fn step(&mut self) -> Option<Arc<EvalNode>> {
        let node = self.queue.pop()?;
        // println!("EvalState::step:\n{}", node.board);
        // println!("EvalState::step:");
        if matches!(node.base_evaluation, Evaluation::Normal(_)) && node.depth > 0 {
            for m in node.board.find_moves().0.into_iter() {
                let board = node.board.apply_move(m);
                let node = EvalNode::new(node.depth - 1, board, Some(node.clone()));
                self.queue.push(node.into())
            }
        }
        node.update_parents();
        Some(node)
    }

    pub fn evaluation(&self) -> Evaluation {
        self.root.evaluation()
    }
}

pub fn recursive_evaluate(depth: usize, board: &BoardState) -> Evaluation {
    let mut state = EvalState::new(depth, board.clone());
    for _ in 0..1000 {
        if let Some(_node) = state.step() {
            continue;
        } else {
            break;
        }
    }
    state.evaluation()
}

/// evaluation base case.
/// looks at the number of moves, etc.
pub fn base_evaluate(board: &BoardState) -> Evaluation {
    run_all_strategies(board)
        .iter()
        .map(|(_s, e)| e)
        .cloned()
        .reduce(|a, b| a & b)
        .unwrap()
}

pub fn run_all_strategies(board: &BoardState) -> Vec<(Strategy, Evaluation)> {
    Strategy::STRATEGIES
        .iter()
        .map(|e| {
            let result = e.score(board);
            (*e, result)
        })
        .collect()
}
