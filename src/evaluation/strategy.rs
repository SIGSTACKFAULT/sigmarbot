use crate::{BoardState, Cardinal, Element, Evaluation};

#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
pub enum Strategy {
    SaltParity,
    CountMoves,
    CountAll,
    CountAvailable,
    Metals,
}

impl Strategy {
    pub const STRATEGIES: &'static [Strategy] = &[
        Strategy::SaltParity,
        Strategy::CountMoves,
        Strategy::CountAvailable,
        Strategy::Metals,
        Strategy::CountAll,
    ];

    pub fn score(self, board: &BoardState) -> Evaluation {
        use Strategy::*;
        let moves = board.find_moves();
        match self {
            SaltParity => {
                let salt_count = board.iter_element(Element::Salt).count();
                let off_parity = {
                    let air = board.iter_element(Cardinal::Air).count() % 2;
                    let fire = board.iter_element(Cardinal::Fire).count() % 2;
                    let water = board.iter_element(Cardinal::Water).count() % 2;
                    let earth = board.iter_element(Cardinal::Earth).count() % 2;
                    air + fire + water + earth
                };
                if salt_count < off_parity {
                    Evaluation::LOSE
                } else {
                    Evaluation::Normal(-(off_parity as i32))
                }
            }
            CountMoves => {
                let count = moves.len();
                if count == 0 {
                    if board.is_empty() {
                        Evaluation::WIN
                    } else {
                        Evaluation::LOSE
                    }
                } else {
                    Evaluation::Normal(count as i32) * 2
                }
            }
            CountAll => Evaluation::Normal(54 - board.len() as i32),
            CountAvailable => Evaluation::Normal(board.iter_available().count() as i32),
            Metals => {
                let metal = board.iter().filter(|(_, e)| e.inner().is_metal()).count() as i32;
                let quicksilver_available = board
                    .iter_available()
                    .filter(|(_, e)| e.is_quicksilver())
                    .count() as i32;

                Evaluation::Normal(quicksilver_available * 2 + (6 - metal))
            }
        }
    }
}
