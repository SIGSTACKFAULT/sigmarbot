use bevy::prelude::*;
use bevy_inspector_egui::bevy_egui::{self, egui};

use crate::BoardState;

use super::run_all_strategies;

pub struct DisplayEvals;
impl Plugin for DisplayEvals {
    fn build(&self, app: &mut App) {
        //
        app.add_systems(Update, display_evals);
    }
}

fn display_evals(mut egui: bevy_egui::EguiContexts, board: Res<BoardState>) {
    egui::Window::new("Evaluations").show(egui.ctx_mut(), |ui| {
        use egui_extras::{Column, TableBuilder};
        let results = run_all_strategies(&board);
        let total = results.iter().map(|(_, e)| e).cloned().reduce(|a, b| a & b);
        TableBuilder::new(ui)
            .column(Column::auto().at_least(100.))
            .column(Column::auto().at_least(100.))
            .body(|mut body| {
                for (evaluator, evaluation) in results {
                    body.row(20., |mut row| {
                        row.col(|ui| {
                            ui.label(format!("{:?}", evaluator));
                        });
                        row.col(|ui| {
                            ui.label(format!("{:?}", evaluation));
                        });
                    })
                }
            });
        if let Some(total) = total {
            ui.label(format!("Total: {:?}", total));
        }
    });
}
