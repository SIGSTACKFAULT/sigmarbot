type EvaluationNumber = i32;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Evaluation {
    Lose,
    Normal(EvaluationNumber),
    Win,
}

impl Evaluation {
    pub const LOSE: Evaluation = Evaluation::Lose;
    pub const ZERO: Evaluation = Evaluation::Normal(0);
    pub const WIN: Evaluation = Evaluation::Win;

    pub fn should_short_circuit(self) -> bool {
        match self {
            Evaluation::Lose | Evaluation::Win => true,
            Evaluation::Normal(_) => false,
        }
    }
}

impl From<Evaluation> for EvaluationNumber {
    fn from(value: Evaluation) -> Self {
        match value {
            Evaluation::Lose => EvaluationNumber::MIN,
            Evaluation::Normal(n) => n,
            Evaluation::Win => EvaluationNumber::MAX,
        }
    }
}

impl From<EvaluationNumber> for Evaluation {
    fn from(value: EvaluationNumber) -> Self {
        Evaluation::Normal(value)
    }
}

impl std::ops::BitOr for Evaluation {
    type Output = Evaluation;
    fn bitor(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Evaluation::Win, _) | (_, Evaluation::Win) => Evaluation::Win,
            (Evaluation::Normal(lhs), Evaluation::Normal(rhs)) => {
                Evaluation::Normal(EvaluationNumber::saturating_add(lhs, rhs))
            }
            (Evaluation::Lose, Evaluation::Normal(x)) => Evaluation::Normal(x),
            (Evaluation::Normal(x), Evaluation::Lose) => Evaluation::Normal(x),
            (Evaluation::Lose, Evaluation::Lose) => Evaluation::Lose,
        }
    }
}

impl std::ops::BitAnd for Evaluation {
    type Output = Evaluation;
    fn bitand(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            (Evaluation::Win, _) | (_, Evaluation::Win) => Evaluation::Win,
            (Evaluation::Normal(lhs), Evaluation::Normal(rhs)) => {
                Evaluation::Normal(EvaluationNumber::saturating_add(lhs, rhs))
            }
            (Evaluation::Lose, Evaluation::Normal(_x)) => Evaluation::Lose,
            (Evaluation::Normal(_x), Evaluation::Lose) => Evaluation::Lose,
            (Evaluation::Lose, Evaluation::Lose) => Evaluation::Lose,
        }
    }
}

impl std::ops::Add<EvaluationNumber> for Evaluation {
    type Output = Evaluation;
    fn add(self, rhs: EvaluationNumber) -> Self::Output {
        match self {
            Evaluation::Normal(n) => Evaluation::Normal(n.saturating_mul(rhs)),
            other => other,
        }
    }
}

impl std::ops::AddAssign<EvaluationNumber> for Evaluation {
    fn add_assign(&mut self, rhs: EvaluationNumber) {
        *self = *self + rhs;
    }
}

impl std::ops::Mul<EvaluationNumber> for Evaluation {
    type Output = Evaluation;
    fn mul(self, rhs: EvaluationNumber) -> Self::Output {
        match self {
            Evaluation::Normal(n) => Evaluation::Normal(n.saturating_mul(rhs)),
            other => other,
        }
    }
}

impl std::ops::MulAssign<EvaluationNumber> for Evaluation {
    fn mul_assign(&mut self, rhs: EvaluationNumber) {
        *self = *self * rhs;
    }
}

impl std::ops::Div<EvaluationNumber> for Evaluation {
    type Output = Evaluation;
    fn div(self, rhs: EvaluationNumber) -> Self::Output {
        match self {
            Self::Normal(x) => Self::Normal(x / rhs),
            other => other,
        }
    }
}

impl PartialOrd for Evaluation {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(Ord::cmp(self, other))
    }
}

impl Ord for Evaluation {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let lhs: i32 = (*self).into();
        let rhs: i32 = (*other).into();
        lhs.cmp(&rhs)
    }
}
