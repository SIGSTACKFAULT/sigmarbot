use crate::{evaluation::Evaluation, BoardState};
use bevy::utils::default;
use bevy::utils::smallvec::SmallVec;
use std::cell::RefCell;
use std::sync::Arc;

use super::base_evaluate;

#[derive(Debug)]
pub struct EvalNode {
    /// counts *down*
    pub depth: usize,
    pub board: BoardState,
    pub parent: Option<Arc<EvalNode>>,
    pub parent_index: RefCell<Option<u8>>,
    pub base_evaluation: Evaluation,
    pub child_evaluations: RefCell<SmallVec<[Evaluation; 10]>>,
}

impl EvalNode {
    pub fn new(depth: usize, board: BoardState, parent: Option<Arc<EvalNode>>) -> EvalNode {
        let base_evaluation = base_evaluate(&board);
        EvalNode {
            depth,
            board,
            parent,
            parent_index: RefCell::new(None),
            base_evaluation,
            child_evaluations: default(),
        }
    }

    pub fn evaluation(&self) -> Evaluation {
        let borrow = self.child_evaluations.borrow();
        let sum = borrow
            .iter()
            .cloned()
            .reduce(|a, b| a | b)
            .unwrap_or(Evaluation::ZERO)
            | self.base_evaluation;
        let count = borrow.len();
        sum / (count + 1) as i32
    }

    pub fn update_parents(&self) {
        if let Some(parent_node) = self.parent.as_ref() {
            let our_evaluation = self.evaluation();
            let mut parent_evaluations = parent_node.child_evaluations.borrow_mut();
            let mut parent_index = self.parent_index.borrow_mut();
            if let Some(parent_index) = parent_index.as_ref() {
                parent_evaluations[usize::from(parent_index.to_owned())] = our_evaluation;
            } else {
                parent_evaluations.push(our_evaluation);
                *parent_index = Some(parent_evaluations.len() as u8 - 1);
            }
            drop(parent_evaluations);
            drop(parent_index);
            parent_node.update_parents();
        }
    }

    pub fn priority(&self) -> i32 {
        // i32::from(self.base_evaluation)
        0
    }
}

impl std::cmp::PartialEq for EvalNode {
    fn eq(&self, other: &Self) -> bool {
        self.priority() == other.priority()
    }
}

impl std::cmp::Eq for EvalNode {}

impl PartialOrd for EvalNode {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(Ord::cmp(self, other))
    }
}

impl Ord for EvalNode {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        Ord::cmp(&self.priority(), &other.priority())
    }
}
