mod board;
// mod click_hex;
mod click_hex;
mod convert;
mod display;
mod evaluation;
mod kill_switch;
mod moves;
mod reference_images;
mod vision;

#[cfg(test)]
mod test;

use bevy::utils::tracing;
use bevy::{prelude::*, window::Cursor};
use hexx::{Hex, HexLayout};
use kill_switch::KillSwitch;
use rayon::iter::{ParallelBridge, ParallelIterator};
use reference_images::ReferenceImages;
use vision::BoardScreenshot;
use xcap::image;

pub use board::{BoardState, Cardinal, CellState, Element, LifeDeath, Metal};
pub use evaluation::Evaluation;
pub use moves::Move;
pub use moves::ScoredMove;
pub use vision::image_diff::*;

const LAYOUT_IN_BEVY_SPACE: HexLayout = hexx::HexLayout {
    orientation: hexx::HexOrientation::Pointy,
    hex_size: Vec2::new(38.1, 38.15),
    invert_x: false,
    invert_y: false,
    origin: Vec2::ZERO,
};

const LAYOUT_IN_MAGNUM_SPACE: HexLayout = hexx::HexLayout {
    orientation: hexx::HexOrientation::Pointy,
    hex_size: Vec2::splat(38.),
    invert_x: false,
    invert_y: true,
    origin: Vec2::new(1217., 506.),
};

fn main() {
    let window = Window {
        transparent: true,
        // decorations: false,
        composite_alpha_mode: bevy::window::CompositeAlphaMode::PreMultiplied,
        title: "SigmarBot\u{2122}".to_string(),
        resizable: false,
        window_level: bevy::window::WindowLevel::AlwaysOnTop,
        position: WindowPosition::Centered(MonitorSelection::Primary),
        cursor: Cursor {
            hit_test: false,
            ..default()
        },
        ..default()
    };
    // window.set_maximized(true);
    {
        App::new()
            .add_plugins(DefaultPlugins.set(WindowPlugin {
                primary_window: Some(window),
                ..default()
            }))
            // .add_plugins(bevy_inspector_egui::quick::WorldInspectorPlugin::new())
            .add_plugins(bevy_inspector_egui::bevy_egui::EguiPlugin)
            .add_plugins(kill_switch::KillSwitchPlugin)
            .add_plugins(board::BoardPlugin)
            .init_resource::<reference_images::ReferenceImages>()
            .add_plugins(display::DisplayPlugin {
                move_lines: true,
                ..default()
            })
            .add_plugins(evaluation::DisplayEvals)
            .add_plugins(moves::MovesPlugin)
            .add_plugins(vision::Vision)
            .add_plugins(click_hex::ClickHexPlugin)
            .insert_resource(ClearColor(Color::NONE))
            .insert_resource(Time::<Fixed>::from_hz(1.))
            .add_systems(Startup, (spawn_camera,))
            .add_systems(
                FixedUpdate,
                (determine_elements)
                    .chain()
                    .after(apply_state_transition::<KillSwitch>)
                    .run_if(KillSwitch::not_inhibit),
            )
    }
    .run();
}

fn spawn_camera(mut commands: Commands) {
    let mut cam = Camera2dBundle::default();
    cam.projection.viewport_origin = Vec2::new(0.700, 0.498);
    commands.spawn(cam);
}

#[tracing::instrument(skip_all)]
fn determine_elements(
    //
    screenshot: Res<BoardScreenshot>,
    references: Res<ReferenceImages>,
    mut board: ResMut<BoardState>,
) {
    let Some(screenshot) = screenshot.0.as_ref() else {
        return;
    };
    for (hex, result) in BoardState::hexes()
        .par_bridge()
        .map(|hex| {
            let cell = get_hex_area(hex, screenshot);
            (hex, vision::identify(hex, cell, &*references))
        })
        .collect::<Vec<_>>()
    {
        match result {
            Some(cell) => board.insert(hex, cell),
            None => board.remove(hex),
        }
    }
}

#[tracing::instrument(skip(screenshot))]
pub fn get_hex_area<'a>(
    hex: Hex,
    screenshot: &'a image::RgbaImage,
) -> image::SubImage<&'a image::RgbaImage> {
    use xcap::image::GenericImageView;
    // let mut buffer = image::RgbaImage::new(32, 32);
    let Vec2 { x, y } = LAYOUT_IN_MAGNUM_SPACE.hex_to_world_pos(hex);
    let x = (x as u32) - 15;
    let y = (y as u32) - 15;
    screenshot.view(x, y, 32, 32)
}
