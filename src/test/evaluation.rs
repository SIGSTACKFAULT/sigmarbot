use crate::evaluation::Evaluation;

#[test]
pub fn evaluation_math() {
    assert_eq!(
        Evaluation::LOSE | Evaluation::Normal(1337),
        Evaluation::Normal(1337)
    );
    assert_eq!(
        Evaluation::LOSE & Evaluation::Normal(1337),
        Evaluation::LOSE
    );
}
