use crate::{
    get_hex_area, reference_images::ReferenceImages, vision::identify, BoardState, CellState,
    Element,
};
use bevy::utils::HashMap;
use hexx::Hex;
use xcap::image;

#[test]
/// tests that the empty-cell-identifying heuristic still works
fn empty_board() {
    let references = ReferenceImages::default();
    let screenshot = image::open("./test_boards/empty_board.png")
        .unwrap()
        .to_rgba8();
    for hex in BoardState::hexes() {
        let cell = get_hex_area(hex, &screenshot);
        print!("{hex:?}: ");
        let result = identify(hex, cell, &references);
        assert_eq!(result, None);
    }
}

#[test]
/// tests that the empty-cell-identifying heuristic still works
fn full_board() {
    let references = ReferenceImages::default();
    let screenshot = image::open("./test_boards/full_board.png")
        .unwrap()
        .to_rgba8();
    for hex in BoardState::hexes() {
        let cell = get_hex_area(hex, &screenshot);
        let result = identify(hex, cell, &references);
        assert_ne!(result, None);
    }
}

/// run vision on `board{name}.png` and compare to `board{name}.ron`
fn test_vision(name: &str) {
    let screenshot = image::open(format!("./test_boards/{}.png", name))
        .unwrap()
        .to_rgba8();
    let correct: HashMap<Hex, CellState> = ron::from_str(
        std::fs::read_to_string(format!("./test_boards/{}.ron", name))
            .unwrap()
            .as_str(),
    )
    .unwrap();
    test_element_totals(&correct);
    let mut failures: u32 = 0;
    let references = ReferenceImages::default();
    for hex in BoardState::hexes() {
        let expected = correct.get(&hex).copied();
        let cell = get_hex_area(hex, &screenshot);
        let found = identify(hex, cell, &references);
        if expected != found {
            println!("Expected {hex:2?} to be {expected:?}, but got {found:?}");
            if failures == 0 {
                cell.to_image()
                    .save(format!("images/{},{}.png", hex.x, hex.y))
                    .unwrap();
            }
            failures += 1;
        }
    }
    assert_eq!(failures, 0);
}

/// check the element totals are correct for a starting position
fn test_element_totals(board: &HashMap<Hex, CellState>) {
    if board.len() != 55 {
        return;
    }
    let mut failures = 0;
    for element in Element::iter() {
        let count = board.iter().filter(|(_, e)| e.inner() == element).count();
        let expected = match element {
            Element::Salt => 4,
            Element::Cardinal(_) => 8,
            Element::Metal(_) => 1,
            Element::Quicksilver => 5,
            Element::LifeDeath(_) => 4,
        };
        if count != expected {
            failures += 1;
            println!("expected {expected} {element:?}; found {count}");
        }
    }
    assert_eq!(failures, 0);
}

#[test]
fn board1() {
    test_vision("board1");
}

#[test]
fn board2() {
    test_vision("board2");
}

#[test]
fn board3() {
    test_vision("board3");
}

#[test]
fn board4() {
    test_vision("board4");
}

#[test]
/// computes the average colour of each cell; used for emptyness heuristic.
/// re-run if i ever change average_color or the grid layout
fn average_color() {
    use crate::vision::average_color;
    let screenshot = image::open("./test_boards/empty_board.png")
        .unwrap()
        .to_rgba8();
    let mut map = bevy::utils::HashMap::new();
    for hex in BoardState::hexes() {
        let cell = get_hex_area(hex, &screenshot);
        let color = average_color(cell);
        map.insert(hex, color);
    }
    println!(
        "{}",
        ron::ser::to_string_pretty(&map, ron::ser::PrettyConfig::new().depth_limit(1)).unwrap()
    );
}
